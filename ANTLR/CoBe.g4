/*

Grammar for CoBe Programming Language

Notes below may not be up-to-date with following ANTLR.
For the moment, treat them as an ongoing discussion.

- globals start(), update(), draw() are called by CoBe engine if defined
- variables must be declared before they can be used
    - declaration defines scope
    - redeclaring variables within same scope is ignored (?)
    - assigning to a variable (without `var`) will scan up the lexical tree to find previous usage
- concatenate (`++`) lists, strings, dicts
    - `++` does not modify, but creates new instance of type
    - concat with dicts creates dict with the left operand updated with right, which can overwrite key/val pairs

Ideas to think about adding...

- list comprehension
- conditional expression (ex: `x = 2 if true else 3`)
- pset returns color that was replaced?
    - should print return string that was replaced?
- what happens if "\n" is in print?
    - should wrap to specified `x`?
- how to wrap printed text?
- event callbacks?
    - timer
    - key / mouse events
- how to specify window resolution?
- how to specify window title?

*/

grammar CoBe;

program : vardecl* EOF;

stat
    : statif
    | statloops
    | statbreak | statcont | statreturn
    | statbifs
    | vardecl | varassign
    | fncall
    | '{' stat* '}'     /* immediately applied anonymous function; allows for local scoping */
    ;

statif      : IF exprbool stat (ELSEIF exprbool stat)* (ELSE stat)? ;

statloops   : statforever | statfor | statwhile | statuntil | statdowhile | statdountil;
statforever : FOREVER stat;                             /* infinite loop */
statfor     : FOR 'var'? identifier IN forrange stat;   /* `var` limits scope of id to stat */
statwhile   : WHILE exprbool stat;
statuntil   : UNTIL exprbool stat;
statdowhile : DO stat WHILE exprbool;
statdountil : DO stat UNTIL exprbool;

statcont    : CONTINUE;
statbreak   : BREAK;
statreturn  : RETURN expr?;

statbifs    : bifprint | bifdbg | bifdbgclr | bifcls | bifpset;


forrange
    : exprval       /* must be a list */
    | '[' (valint | '(' expr ')') ',' (valint | '(' expr ')') '..' (valint | '(' expr ')') ']'
    | '[' (valint | '(' expr ')')                          '..' (valint | '(' expr ')') ']'
    | '[' (valint | '(' expr ')') ',' (valint | '(' expr ')') '..'                      ']'
    | '[' (valint | '(' expr ')')                          '..'                      ']'
    ;

fncall : varfull '(' fncallargs ')';
fncallargs
    : fncallposargs (',' fncallnamedargs)?
    | fncallnamedargs
    |
    ;
fncallposargs : expr (',' expr)* ;
fncallnamedargs : identifier '=' expr (',' identifier '=' expr)* ;


vardecl   : 'var' identifier ('=' expr)? (',' identifier ('=' expr)?)*;
varassign : varfull '=' expr;
varfull   : identifier varpart;
varpart
    : '.' (identifier  | '(' expr ')' ) varpart    /* dict */
    | '@' (valint | '(' expr ')' ) varpart    /* list */
    | '[' expr ']' varpart                 /* dict or list */
    |
    ;

idassignlist : identifier '=' expr (',' identifier '=' expr)*;
exprposlist : (valint '=')? expr (',' (valint '=')? expr)*;


expr : exprval | exprbool | bifnewdict | bifnewlist | bifnewfunc;

exprval
    : varfull
    | valnum    | valstr    | valbool | vtype | valnil
    | bifnumber | bifstring | bifbool | biftype
    | biflen | bifminind | bifmaxind | bifkeys | bifinds | bifvals
    | biffloor | bifceil | bifcos | bifsin | biftan | bifatan2
    | bifpset | bifpget
    | '(' expr ')'
    | <assoc=right> exprval opPower exprval
    | exprval opMultDiv exprval
    | exprval opAddSub exprval
    | exprval opMod exprval
    | exprval opConcat exprval
    | '{' stat* '}'     /* immediately applied anonymous function; returns `nil` if nothing returned */
    | fncall
    ;

exprbool
    : '(' exprbool ')'
    | varfull
    | NOT exprbool
    | exprval opCompares exprval
    | exprbool (opAnd exprbool)+
    | exprbool (opOr exprbool)+
    | exprval opContainedIn varfull
    | exprval IS vtype
    ;


/* ****************** */
/* built-in functions */
/* ****************** */

bifnewdict  : DICT '(' idassignlist?  ')' | '[' idassignlist? ']';
bifnewlist  : LIST '(' exprposlist?   ')' | '[' exprposlist ']';
bifnewfunc  : FUNC '(' bifnewfuncargs ')' stat ;
/*
    all parameters are positional and named.
    the optional `*id` and `**id` syntax captures all the parameters passed as list or dict (resp.), which
    are independent of the parameters in function signature!

        `id`        : positional/named param with default value of `nil`
        `id=expr`   : positional/named param with given default value
        `*id`       : all passed parameters in passed order (list)
        `**id`      : all passed named parameters (dict)
*/
bifnewfuncargs
    : identifier (',' identifier)* (',' identifier '=' expr)* ('*' identifier)? ('**' identifier)?
    | identifier '=' expr  (',' identifier '=' expr)* ('*' identifier)? ('**' identifier)?
    | ('*' identifier)? ('**' identifier)?
    ;

bifdbg    : DBG  '(' expr ')';                              /* s */
bifdbgclr : DBGCLR '(' ')';
bifprint  : PRINT  '(' (expr (',' fncallnamedargs)?)? ')';  /* s, [xy], [c] */

bifcls    : CLS '(' fncallnamedargs ')';                    /* [c] */
bifpset   : PSET '(' expr ',' expr ',' expr ')';            /* x, y, c*/
bifpget   : PGET '(' expr ',' expr ')';                     /* x, y */

biflen    : LEN    '(' varfull ')' | '#' varfull;
bifminind : MININD '(' varfull ')';
bifmaxind : MAXIND '(' varfull ')';
bifkeys   : KEYS   '(' varfull ')';
bifinds   : INDS   '(' varfull ')';
bifvals   : VALS   '(' varfull ')';

biffloor  : FLOOR  '(' expr ')';
bifceil   : CEIL   '(' expr ')';
bifcos    : COS    '(' expr ')';
bifsin    : SIN    '(' expr ')';
biftan    : TAN    '(' expr ')';
bifatan2  : ATAN2  '(' expr ',' expr ')';

biftype   : TYPE   '(' expr ')';    /* returns type of expr */
bifnumber : NUMBER '(' expr ')';    /* returns expr (ex: string) as number */
bifstring : STRING '(' expr ')';    /* returns expr (ex: number) as string */
bifbool   : BOOL   '(' expr ')';    /* returns expr (ex: number) as bool */

vtype : NUMBER | STRING | BOOL | NIL | FUNC | DICT | LIST | TYPE;

opMultDiv   : MULT | DIV | IDIV;
opAddSub    : ADD | SUB;
opPower     : POWER;
opMod       : MOD;
opCompares  : LESSER | GREATER | NGREATER | NLESSER | NEQUAL | EQUAL ;
opAnd       : AND;
opOr        : OR;
opConcat    : CONCAT;
opContainedIn : VALIN | KEYIN | INDIN ;

identifier : IDENTIFIER;

valnum : valint | valfloat;
valint : '-'? INT;
valfloat
    : '-'? INT '.' INT? floatexp?
    | '-'?     '.' INT  floatexp?
    | '-'? INT          floatexp
    ;
floatexp : ('e' | 'E') ('+' | '-')? INT;
valstr  : NORMALSTRING | LONGSTRING;
valnil  : NIL;
valbool : TRUE | FALSE;


fragment
Digit : [0-9];

fragment
EscapeSequence
    : '\\' [abfnrtvz"'\\]
    | '\\' '\r'? '\n'
    ;

/* container data types */
FUNC : 'function';
DICT : 'dict';
LIST : 'list';
STRING : 'string';
NORMALSTRING : '"' ( EscapeSequence | ~('\\'|'"') )* '"';
LONGSTRING   : '"""' .*? '"""';

/* value data types */
NUMBER : 'number';
BOOL   : 'bool';
TYPE   : 'type';
IS     : 'is';
TRUE   : 'true';
FALSE  : 'false';
NIL    : 'nil';
INT    : Digit+ ;

/* printing/reporting functions */
PRINT  : 'print';
DBG    : 'dbg';
DBGCLR : 'dbgclr';

/* misc */
RETURN : 'return';

/* graphic functions */
CLS  : 'cls';
PSET : 'pset';
PGET : 'pget';

/* loops */
FOR      : 'for';
IN       : 'in';
WHILE    : 'while';
UNTIL    : 'until';
DO       : 'do';
FOREVER  : 'forever';
BREAK    : 'break';
CONTINUE : 'continue';

/* conditionals */
IF     : 'if';
ELSEIF : 'elseif';
ELSE   : 'else';

/* infix data operators */
CONCAT : '++';
KEYIN  : 'keyin';
INDIN  : 'indexin' | 'indin';
VALIN  : 'valuein' | 'valin';

/* infix boolean operators */
AND : 'and' | '&';
OR  : 'or'  | '|';
NOT : 'not' | '!';

/* infix arithmetic operators */
ADD   : '+';
SUB   : '-';
MULT  : '*';
DIV   : '/';
IDIV  : '//';
POWER : '^';
MOD   : '%' | 'mod';

/* comparators */
EQUAL    : '==';
NEQUAL   : '!=';
GREATER  : '>';
NLESSER  : '>=';
LESSER   : '<';
NGREATER : '<=';

/* container functions */
LEN    : 'len';
MININD : 'minindex' | 'minind';
MAXIND : 'maxindex' | 'maxind';
KEYS   : 'keys';
VALS   : 'values'  | 'vals';
INDS   : 'indices' | 'inds';

/* arithmetic functions */
FLOOR  : 'floor';
CEIL   : 'ceiling' | 'ceil';
COS    : 'cos';
SIN    : 'sin';
TAN    : 'tan';
ATAN2  : 'atan2';

IDENTIFIER : [a-zA-Z_][a-zA-Z_0-9]*;

COMMENTSL : '//' .*? ('\r\n'|'\r'|'\n'|EOF) -> channel(HIDDEN);
COMMENTML : '/*' .*? '*/'                   -> channel(HIDDEN);

WS : [ \t\r\n]+ -> skip;


