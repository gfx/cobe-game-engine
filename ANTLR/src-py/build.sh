#!/bin/bash -e

rm -f CoBe.interp CoBe.tokens
rm -f CoBeLexer.interp CoBeLexer.py CoBeLexer.tokens
rm -f CoBeParser.py
rm -f CoBeVisitor.py

antlr4 -Dlanguage=Python3 -no-listener -visitor CoBe.g4
