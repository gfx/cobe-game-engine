#!/bin/bash -e

rm -f CoBe.interp CoBe.tokens
rm -f CoBeLexer.interp CoBeLexer.js CoBeLexer.tokens
rm -f CoBeParser.js
rm -f CoBeVisitor.js

antlr4 -Dlanguage=JavaScript -no-listener -visitor CoBe.g4
